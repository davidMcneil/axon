//===-- AxonSelectionDAGInfo.cpp - Axon SelectionDAG Info ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements the AxonSelectionDAGInfo class.
//
//===----------------------------------------------------------------------===//

#include "AxonSelectionDAGInfo.h"
using namespace llvm;

#define DEBUG_TYPE "axon-selectiondag-info"

AxonSelectionDAGInfo::~AxonSelectionDAGInfo() {}
