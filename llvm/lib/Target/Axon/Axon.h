//===-- Axon.h - Top-level interface for Axon representation --*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the entry points for global functions defined in the LLVM
// Axon back-end.
//
//===----------------------------------------------------------------------===//

#ifndef TARGET_AXON_H
#define TARGET_AXON_H

#include "MCTargetDesc/AxonMCTargetDesc.h"
#include "llvm/Target/TargetMachine.h"

namespace llvm {
class TargetMachine;
class AxonTargetMachine;

FunctionPass *createAxonISelDag(AxonTargetMachine &TM,
                               CodeGenOpt::Level OptLevel);
} // end namespace llvm;

#endif
