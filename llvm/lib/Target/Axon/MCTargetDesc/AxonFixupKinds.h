//===-- AxonFixupKinds.h - Axon-Specific Fixup Entries ------------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_AxonFIXUPKINDS_H
#define LLVM_AxonFIXUPKINDS_H

#include "llvm/MC/MCFixup.h"

namespace llvm {
namespace Axon {
enum Fixups {
  fixup_axon_mov_hi16_pcrel = FirstTargetFixupKind,
  fixup_axon_mov_lo16_pcrel,

  // Marker
  LastTargetFixupKind,
  NumTargetFixupKinds = LastTargetFixupKind - FirstTargetFixupKind
};
}
}

#endif

