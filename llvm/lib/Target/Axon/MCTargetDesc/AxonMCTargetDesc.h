//===-- AxonMCTargetDesc.h - Axon Target Descriptions ---------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file provides Axon specific target descriptions.
//
//===----------------------------------------------------------------------===//

#ifndef AxonMCTARGETDESC_H
#define AxonMCTARGETDESC_H

#include "llvm/Support/DataTypes.h"

namespace llvm {
class Target;
class MCInstrInfo;
class MCRegisterInfo;
class MCSubtargetInfo;
class MCContext;
class MCCodeEmitter;
class MCAsmInfo;
class MCCodeGenInfo;
class MCInstPrinter;
class MCObjectWriter;
class MCAsmBackend;

class StringRef;
class raw_ostream;
class raw_pwrite_stream;
class Triple;

extern Target TheAxonTarget;

MCCodeEmitter *createAxonMCCodeEmitter(const MCInstrInfo &MCII,
                                      const MCRegisterInfo &MRI,
                                      MCContext &Ctx);

MCAsmBackend *createAxonAsmBackend(const Target &T, const MCRegisterInfo &MRI,
                                  const Triple &TT, StringRef CPU);

MCObjectWriter *createAxonELFObjectWriter(raw_pwrite_stream &OS, uint8_t OSABI);

} // End llvm namespace

// Defines symbolic names for Axon registers.  This defines a mapping from
// register name to register number.
//
#define GET_REGINFO_ENUM
#include "AxonGenRegisterInfo.inc"

// Defines symbolic names for the Axon instructions.
//
#define GET_INSTRINFO_ENUM
#include "AxonGenInstrInfo.inc"

#define GET_SUBTARGETINFO_ENUM
#include "AxonGenSubtargetInfo.inc"

#endif
