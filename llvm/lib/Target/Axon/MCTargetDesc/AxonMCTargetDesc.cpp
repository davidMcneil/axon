//===-- AxonMCTargetDesc.cpp - Axon Target Descriptions -----------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file provides Axon specific target descriptions.
//
//===----------------------------------------------------------------------===//

#include "AxonMCTargetDesc.h"
#include "InstPrinter/AxonInstPrinter.h"
#include "AxonMCAsmInfo.h"
#include "llvm/MC/MCCodeGenInfo.h"
#include "llvm/MC/MCInstrInfo.h"
#include "llvm/MC/MCRegisterInfo.h"
#include "llvm/MC/MCSubtargetInfo.h"
#include "llvm/MC/MCStreamer.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/FormattedStream.h"
#include "llvm/Support/TargetRegistry.h"

#define GET_INSTRINFO_MC_DESC
#include "AxonGenInstrInfo.inc"

#define GET_SUBTARGETINFO_MC_DESC
#include "AxonGenSubtargetInfo.inc"

#define GET_REGINFO_MC_DESC
#include "AxonGenRegisterInfo.inc"

using namespace llvm;

static MCInstrInfo *createAxonMCInstrInfo() {
  MCInstrInfo *X = new MCInstrInfo();
  InitAxonMCInstrInfo(X);
  return X;
}

static MCRegisterInfo *createAxonMCRegisterInfo(const Triple &TT) {
  MCRegisterInfo *X = new MCRegisterInfo();
  InitAxonMCRegisterInfo(X, Axon::RA);
  return X;
}

static MCSubtargetInfo *createAxonMCSubtargetInfo(const Triple &TT,
                                                 StringRef CPU,
                                                 StringRef FS) {
  return createAxonMCSubtargetInfoImpl(TT, CPU, FS);
}

static MCAsmInfo *createAxonMCAsmInfo(const MCRegisterInfo &MRI,
                                     const Triple &TT) {
  return new AxonMCAsmInfo(TT);
}

static MCCodeGenInfo *createAxonMCCodeGenInfo(const Triple &TT, Reloc::Model RM,
                                             CodeModel::Model CM,
                                             CodeGenOpt::Level OL) {
  MCCodeGenInfo *X = new MCCodeGenInfo();
  if (RM == Reloc::Default) {
    RM = Reloc::Static;
  }
  if (CM == CodeModel::Default) {
    CM = CodeModel::Small;
  }
  if (CM != CodeModel::Small && CM != CodeModel::Large) {
    report_fatal_error("Target only supports CodeModel Small or Large");
  }

  X->initMCCodeGenInfo(RM, CM, OL);
  return X;
}

static MCInstPrinter *
createAxonMCInstPrinter(const Triple &TT, unsigned SyntaxVariant,
                       const MCAsmInfo &MAI, const MCInstrInfo &MII,
                       const MCRegisterInfo &MRI) {
  return new AxonInstPrinter(MAI, MII, MRI);
}

// Force static initialization.
extern "C" void LLVMInitializeAxonTargetMC() {
  // Register the MC asm info.
  RegisterMCAsmInfoFn X(TheAxonTarget, createAxonMCAsmInfo);

  // Register the MC codegen info.
  TargetRegistry::RegisterMCCodeGenInfo(TheAxonTarget, createAxonMCCodeGenInfo);

  // Register the MC instruction info.
  TargetRegistry::RegisterMCInstrInfo(TheAxonTarget, createAxonMCInstrInfo);

  // Register the MC register info.
  TargetRegistry::RegisterMCRegInfo(TheAxonTarget, createAxonMCRegisterInfo);

  // Register the MC subtarget info.
  TargetRegistry::RegisterMCSubtargetInfo(TheAxonTarget,
                                          createAxonMCSubtargetInfo);

  // Register the MCInstPrinter
  TargetRegistry::RegisterMCInstPrinter(TheAxonTarget, createAxonMCInstPrinter);

  // Register the ASM Backend.
  TargetRegistry::RegisterMCAsmBackend(TheAxonTarget, createAxonAsmBackend);

  // Register the MCCodeEmitter
  TargetRegistry::RegisterMCCodeEmitter(TheAxonTarget, createAxonMCCodeEmitter);
}
