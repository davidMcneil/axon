//===-- AxonMCAsmInfo.h - Axon asm properties --------------------*- C++ -*--===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the declaration of the AxonMCAsmInfo class.
//
//===----------------------------------------------------------------------===//

#ifndef AxonTARGETASMINFO_H
#define AxonTARGETASMINFO_H

#include "llvm/MC/MCAsmInfoELF.h"

namespace llvm {
class StringRef;
class Target;
class Triple;

class AxonMCAsmInfo : public MCAsmInfoELF {
  virtual void anchor();

public:
  explicit AxonMCAsmInfo(const Triple &TT);
};

} // namespace llvm

#endif
