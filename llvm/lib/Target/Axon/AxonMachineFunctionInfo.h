//===-- AxonMachineFuctionInfo.h - Axon machine function info -*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file declares Axon-specific per-machine-function information.
//
//===----------------------------------------------------------------------===//

#ifndef AxonMACHINEFUNCTIONINFO_H
#define AxonMACHINEFUNCTIONINFO_H

#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineFunction.h"

namespace llvm {

// Forward declarations
class Function;

/// AxonFunctionInfo - This class is derived from MachineFunction private
/// Axon target-specific information for each MachineFunction.
class AxonFunctionInfo : public MachineFunctionInfo {
public:
  AxonFunctionInfo() {}

  ~AxonFunctionInfo() {}
};
} // End llvm namespace

#endif // AxonMACHINEFUNCTIONINFO_H

