//===-- AxonRegisterInfo.cpp - Axon Register Information ----------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file contains the Axon implementation of the MRegisterInfo class.
//
//===----------------------------------------------------------------------===//

#include "AxonRegisterInfo.h"
#include "Axon.h"
#include "AxonFrameLowering.h"
#include "AxonInstrInfo.h"
#include "AxonMachineFunctionInfo.h"
#include "llvm/ADT/BitVector.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/CodeGen/MachineFrameInfo.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/MachineModuleInfo.h"
#include "llvm/CodeGen/MachineRegisterInfo.h"
#include "llvm/CodeGen/RegisterScavenging.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Type.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/ErrorHandling.h"
#include "llvm/Support/MathExtras.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Target/TargetFrameLowering.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Target/TargetOptions.h"

#define GET_REGINFO_TARGET_DESC
#include "AxonGenRegisterInfo.inc"

using namespace llvm;

AxonRegisterInfo::AxonRegisterInfo() : AxonGenRegisterInfo(Axon::RA) {}

const uint16_t *
AxonRegisterInfo::getCalleeSavedRegs(const MachineFunction *MF) const {
  static const uint16_t CalleeSavedRegs[] = { Axon::S0, Axon::S1, Axon::S2, Axon::S3, Axon::S4, Axon::S5, Axon::S6, Axon::S7,
                                              Axon::RA, 0 };
  return CalleeSavedRegs;
}

BitVector AxonRegisterInfo::getReservedRegs(const MachineFunction &MF) const {
  BitVector Reserved(getNumRegs());

  Reserved.set(Axon::SP);
  Reserved.set(Axon::RA);
  return Reserved;
}

const uint32_t *AxonRegisterInfo::getCallPreservedMask(const MachineFunction &MF,
                                                      CallingConv::ID) const {
  return CC_Save_RegMask;
}

bool
AxonRegisterInfo::requiresRegisterScavenging(const MachineFunction &MF) const {
  return true;
}

bool
AxonRegisterInfo::trackLivenessAfterRegAlloc(const MachineFunction &MF) const {
  return true;
}

bool AxonRegisterInfo::useFPForScavengingIndex(const MachineFunction &MF) const {
  return false;
}

void AxonRegisterInfo::eliminateFrameIndex(MachineBasicBlock::iterator II,
                                          int SPAdj, unsigned FIOperandNum,
                                          RegScavenger *RS) const {
  MachineInstr &MI = *II;
  const MachineFunction &MF = *MI.getParent()->getParent();
  const MachineFrameInfo *MFI = MF.getFrameInfo();
  MachineOperand &FIOp = MI.getOperand(FIOperandNum);
  unsigned FI = FIOp.getIndex();

  // Determine if we can eliminate the index from this kind of instruction.
  unsigned ImmOpIdx = 0;
  switch (MI.getOpcode()) {
  default:
    // Not supported yet.
    return;
  case Axon::LDR:
  case Axon::STR:
    ImmOpIdx = FIOperandNum + 1;
    break;
  }

  // FIXME: check the size of offset.
  MachineOperand &ImmOp = MI.getOperand(ImmOpIdx);
  int Offset = (MFI->getObjectOffset(FI) + MFI->getStackSize() + ImmOp.getImm());
  FIOp.ChangeToRegister(Axon::SP, false);
  ImmOp.setImm(Offset);
}

unsigned AxonRegisterInfo::getFrameRegister(const MachineFunction &MF) const {
  return Axon::SP;
}
