//===-- AxonTargetMachine.cpp - Define TargetMachine for Axon -----------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
//
//===----------------------------------------------------------------------===//

#include "AxonTargetMachine.h"
#include "Axon.h"
#include "AxonFrameLowering.h"
#include "AxonInstrInfo.h"
#include "AxonISelLowering.h"
#include "AxonSelectionDAGInfo.h"
#include "llvm/CodeGen/Passes.h"
#include "llvm/CodeGen/TargetLoweringObjectFileImpl.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/PassManager.h"
#include "llvm/Support/TargetRegistry.h"

using namespace llvm;

static std::string computeDataLayout(const Triple &TT, StringRef CPU,
                                     const TargetOptions &Options) {
  // XXX Build the triple from the arguments.
  // This is hard-coded for now for this example target.
  return "E-p:32:32-i32:32-f32:32:32";
}

AxonTargetMachine::AxonTargetMachine(const Target &T, const Triple &TT,
                                   StringRef CPU, StringRef FS,
                                   const TargetOptions &Options,
                                   Reloc::Model RM, CodeModel::Model CM,
                                   CodeGenOpt::Level OL)
    : LLVMTargetMachine(T, computeDataLayout(TT, CPU, Options), TT, CPU, FS,
                        Options, RM, CM, OL),
      Subtarget(TT, CPU, FS, *this),
      TLOF(make_unique<TargetLoweringObjectFileELF>()) {
  initAsmInfo();
}

namespace {
/// Axon Code Generator Pass Configuration Options.
class AxonPassConfig : public TargetPassConfig {
public:
  AxonPassConfig(AxonTargetMachine *TM, PassManagerBase &PM)
      : TargetPassConfig(TM, PM) {}

  AxonTargetMachine &getAxonTargetMachine() const {
    return getTM<AxonTargetMachine>();
  }

  virtual bool addPreISel() override;
  virtual bool addInstSelector() override;
  virtual void addPreEmitPass() override;
};
} // namespace

TargetPassConfig *AxonTargetMachine::createPassConfig(PassManagerBase &PM) {
  return new AxonPassConfig(this, PM);
}

bool AxonPassConfig::addPreISel() { return false; }

bool AxonPassConfig::addInstSelector() {
  addPass(createAxonISelDag(getAxonTargetMachine(), getOptLevel()));
  return false;
}

void AxonPassConfig::addPreEmitPass() {}

// Force static initialization.
extern "C" void LLVMInitializeAxonTarget() {
  RegisterTargetMachine<AxonTargetMachine> X(TheAxonTarget);
}
