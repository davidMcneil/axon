//===-- AxonSelectionDAGInfo.h - Axon SelectionDAG Info -------*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file defines the Axon subclass for TargetSelectionDAGInfo.
//
//===----------------------------------------------------------------------===//

#ifndef AxonSELECTIONDAGINFO_H
#define AxonSELECTIONDAGINFO_H

#include "llvm/Target/TargetSelectionDAGInfo.h"

namespace llvm {

class AxonSelectionDAGInfo : public TargetSelectionDAGInfo {
public:
  ~AxonSelectionDAGInfo();
};
}

#endif
