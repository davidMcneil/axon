//===-- AxonTargetMachine.h - Define TargetMachine for Axon ---*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file declares the Axon specific subclass of TargetMachine.
//
//===----------------------------------------------------------------------===//

#ifndef AxonTARGETMACHINE_H
#define AxonTARGETMACHINE_H

#include "Axon.h"
#include "AxonFrameLowering.h"
#include "AxonISelLowering.h"
#include "AxonInstrInfo.h"
#include "AxonSelectionDAGInfo.h"
#include "AxonSubtarget.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/Target/TargetMachine.h"

namespace llvm {

class AxonTargetMachine : public LLVMTargetMachine {
  AxonSubtarget Subtarget;
  std::unique_ptr<TargetLoweringObjectFile> TLOF;

public:
  AxonTargetMachine(const Target &T, const Triple &TT, StringRef CPU,
                   StringRef FS, const TargetOptions &Options, Reloc::Model RM,
                   CodeModel::Model CM, CodeGenOpt::Level OL);
  
  const AxonSubtarget * getSubtargetImpl() const {
    return &Subtarget;
  }
  
  virtual const TargetSubtargetInfo *
  getSubtargetImpl(const Function &) const override {
    return &Subtarget;
  }

  // Pass Pipeline Configuration
  virtual TargetPassConfig *createPassConfig(PassManagerBase &PM) override;
  
  TargetLoweringObjectFile *getObjFileLowering() const override {
    return TLOF.get();
  }
};

} // end namespace llvm

#endif
