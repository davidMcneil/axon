//===-- AxonTargetInfo.cpp - Axon Target Implementation -----------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//

#include "Axon.h"
#include "llvm/IR/Module.h"
#include "llvm/Support/TargetRegistry.h"
using namespace llvm;

Target llvm::TheAxonTarget;

extern "C" void LLVMInitializeAxonTargetInfo() {
  RegisterTarget<Triple::axon> X(TheAxonTarget, "axon", "Axon");
}
