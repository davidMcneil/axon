; ModuleID = 'test/test.bc'
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

@x = global [7 x i32] [i32 5, i32 6, i32 7, i32 -100, i32 89, i32 21, i32 32], align 16 ; [#uses=0 type=[7 x i32]*]

; [#uses=0]
; Function Attrs: nounwind uwtable
define i32 @test0(i32 %i, i32 %j) #0 {
  %1 = alloca i32, align 4                        ; [#uses=2 type=i32*]
  %2 = alloca i32, align 4                        ; [#uses=2 type=i32*]
  store i32 %i, i32* %1, align 4
  store i32 %j, i32* %2, align 4
  %3 = load i32, i32* %1, align 4                 ; [#uses=1 type=i32]
  %4 = load i32, i32* %2, align 4                 ; [#uses=1 type=i32]
  %5 = mul nsw i32 %3, %4                         ; [#uses=1 type=i32]
  ret i32 %5
}

; [#uses=0]
; Function Attrs: nounwind uwtable
define i32 @main() #0 {
  %1 = alloca i32, align 4                        ; [#uses=1 type=i32*]
  %a = alloca i32, align 4                        ; [#uses=2 type=i32*]
  %b = alloca i32, align 4                        ; [#uses=2 type=i32*]
  %c = alloca i32, align 4                        ; [#uses=3 type=i32*]
  store i32 0, i32* %1
  store i32 16, i32* %a, align 4
  store i32 15, i32* %b, align 4
  store i32 0, i32* %c, align 4
  %2 = load i32, i32* %a, align 4                 ; [#uses=1 type=i32]
  %3 = load i32, i32* %b, align 4                 ; [#uses=1 type=i32]
  %4 = icmp sgt i32 %2, %3                        ; [#uses=1 type=i1]
  br i1 %4, label %5, label %6

; <label>:5                                       ; preds = %0
  store i32 1, i32* %c, align 4
  br label %6

; <label>:6                                       ; preds = %5, %0
  %7 = load i32, i32* %c, align 4                 ; [#uses=1 type=i32]
  ret i32 %7
}

attributes #0 = { nounwind uwtable "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+sse,+sse2" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 3.7.0 (tags/RELEASE_370/final)"}
