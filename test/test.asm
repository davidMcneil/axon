	.text
	j main:
test0:                                  # @test0
# BB#0:
	addi sp   sp   -8
	sw   a0   sp   4
	sw   a1   sp   0
	lw   t0   sp   4
	mul  v0   t0   a1
	addi sp   sp   8
	jr   ra

main:                                   # @main
# BB#0:
	addi sp   sp   -16
	loi  t0   0
	sw   t0   sp   12
	loi  t1   16
	sw   t1   sp   8
	loi  t1   15
	sw   t1   sp   4
	sw   t0   sp   0
	lw   t0   sp   8
	lw   t1   sp   4
	slt  at   t1   t0
	beq  at   r0   LBB1_2:
# BB#1:
	loi  t0   1
	sw   t0   sp   0
LBB1_2:
	lw   v0   sp   0
	addi sp   sp   16
	halt
	jr   ra

	.data
x:
	.datum	5                       # 0x5
	.datum	6                       # 0x6
	.datum	7                       # 0x7
	.datum	4294967196              # 0xffffff9c
	.datum	89                      # 0x59
	.datum	21                      # 0x15
	.datum	32                      # 0x20
