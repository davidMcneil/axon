	.text
	.file	"test/test.bc"
	.globl	test0
	.type	test0,@function
test0:                                  # @test0
# BB#0:
	addi sp   sp   -8
	sw   a0   sp   4
	sw   a1   sp   0
	lw   t0   sp   4
	mul  v0   t0   a1
	addi sp   sp   8
	jr   ra
.Lfunc_end0:
	.size	test0, .Lfunc_end0-test0

	.globl	main
	.type	main,@function
main:                                   # @main
# BB#0:
	addi sp   sp   -16
	loi  t0   0
	sw   t0   sp   12
	loi  t1   16
	sw   t1   sp   8
	loi  t1   15
	sw   t1   sp   4
	sw   t0   sp   0
	lw   t0   sp   8
	lw   t1   sp   4
	slt  at   t1   t0
	beq  at   r0   .LBB1_2:
# BB#1:
	loi  t0   1
	sw   t0   sp   0
.LBB1_2:
	lw   v0   sp   0
	addi sp   sp   16
	halt
	jr   ra
.Lfunc_end1:
	.size	main, .Lfunc_end1-main

	.type	x,@object               # @x
	.data
	.globl	x
	.align	16
x:
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	4294967196              # 0xffffff9c
	.long	89                      # 0x59
	.long	21                      # 0x15
	.long	32                      # 0x20
	.size	x, 28


	.ident	"clang version 3.7.0 (tags/RELEASE_370/final)"
	.section	".note.GNU-stack","",@progbits
