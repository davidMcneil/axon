RELDIR   = rel_build
DEVDIR   = dev_build
MKDIR    = mkdir -p

.PHONY: config_rel
config_rel:
	@$(MKDIR) $(RELDIR)
	cmake -DCMAKE_BUILD_TYPE=Release -DCLANG_BUILD_EXAMPLES=ON -DLLVM_BUILD_EXAMPLES=ON -G "Unix Makefiles" -B$(RELDIR) -Hllvm/

.PHONY: build_rel
build_rel:
	make -j8 -l8 -C $(RELDIR)

.PHONY: config_dev
config_dev:
	@$(MKDIR) $(DEVDIR)
	cmake -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_C_COMPILER=clang -DCMAKE_BUILD_TYPE=Release -DLLVM_TARGETS_TO_BUILD=Axon -G "Unix Makefiles" -B$(DEVDIR) -Hllvm/

.PHONY: build_dev
build_dev:
	make -j8 -l8 -C $(DEVDIR)

.PHONY: test
test:
	# View available targets
	./$(DEVDIR)/bin/llc -version
	# Compile program to llvm byte code
	 ./$(DEVDIR)/bin/clang -O0 -c test/test.c -emit-llvm -o test/test.bc
	# Dissasemble llvm byte code
	./$(DEVDIR)/bin/llvm-dis test/test.bc -show-annotations -o test/test.ll
	# Convert llvm byte code to target assembly
	./$(DEVDIR)/bin/llc -march=axon -relocation-model=pic -filetype=asm test/test.bc -o test/test.raw.asm
	# Curate the target assembly
	./curate.py test/test.raw.asm
	# Assemble the target assembly
	../synapse/bin/syn test/test.asm
	# Run the assembly in simulator
	../neurosim/bin/neurosim -sr -f test/test.bin